# Start from GitLab's default job-runner image
FROM gitlab/gitlab-runner:latest

# Update the package lists for upgrades for resync
RUN apt-get update

# Install software-properties-common package
RUN apt-get install -y software-properties-common

# Add ansible PPA (personal package archive) to the system
RUN apt-add-repository --yes --update ppa:ansible/ansible

# Install ansible and unzip packages
RUN apt-get install -y ansible unzip

# Download and install Terraform
RUN wget https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip
RUN unzip terraform_0.12.24_linux_amd64.zip
RUN mv terraform /usr/local/bin/
RUN rm terraform_0.12.24_linux_amd64.zip
